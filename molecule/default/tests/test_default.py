import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_jfrog_cli_version(host):
    cmd = host.run('/usr/local/bin/jfrog --version')
    assert cmd.stdout.strip() == 'jfrog version 1.8.0'


def test_jfrog_cli_config(host):
    cmd = host.run('jfrog rt config show')
    assert 'Server ID: artifactory.esss.lu.se' in cmd.stdout
    assert 'Url: https://artifactory.esss.lu.se/artifactory/' in cmd.stdout
    assert 'API key: my-api-key' in cmd.stdout
